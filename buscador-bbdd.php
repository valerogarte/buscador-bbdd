<?php
/* Asunto: Buscador de textos en BBDD.
/* Descripción: Buscar un texto en todos los campos dentro de las tablas seleccionadas de una Base de Datos local o remota.
/* Fecha: Marzo 2015
/*
/* Daniel Valero González 
/*
/* 
/* http://valerogarte.com/
*/


//Desactivamos la notificación de errores en el script
ini_set('display_errors','0');


//Se inicia la sesion:
session_start();


	//Si hago logout
		if(isset($_GET['salir'])){

			session_unset();
			session_destroy();
			header("Location: ".$_SERVER['PHP_SELF']);
			exit();

		}


?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
<script
  src="http://code.jquery.com/jquery-3.3.1.js"></script>
<style type="text/css">
	*{
		font-family: 'Roboto Slab', serif;
	}

	#manage_tables input {
		border: 2px solid #ff6e6e;
	    padding: 10px;
	    border-radius: 10px;
	    color: #ff6e6e;
	    background: white;
	    cursor: pointer;
	    outline: none;
	}

	#manage_tables input.nocheck {
	    background: #ff6e6e69 !important;
    	color: #fffcd4 !important;
	}
	#manage_tables input:hover {
		background: #fff4f4;
	}
	.salir_bbdd {
	    background: #c1c0c0;
	    text-decoration: none;
	    padding: 5px 10px;
	    position: absolute;
	    border-radius: 10px;
	    color: #fffcd4;
	    right: 10px;
	}
</style>

<?php 

	function remarcar($conexion, $tabla, $campo, $cadena,$strip=TRUE) {
			$cadena_ini = $cadena;
			$palabra_ini = $_POST['palabra'];

			if($strip==TRUE) $cadena=strip_tags($cadena);

			$palabra=strtolower($_POST['palabra']);
			$cadena=str_replace ( $palabra, '<span style="color: #dfff00;">'.$palabra."</span>", $cadena);

			$palabra=ucfirst($palabra);
			$cadena=str_replace ( $palabra, '<span style="color: #dfff00;">'.$palabra."</span>", $cadena);

			$palabra=strtoupper($palabra);
			$cadena=str_replace ( $palabra, '<span style="color: #dfff00;">'.$palabra."</span>", $cadena);

			$pos = strpos($cadena, '<span style="color: #dfff00;">');

			//HACER UPDATE

			if (!($pos === false) && $_SESSION["reemplazar"]!=false ) {
				$cadena=str_replace( $palabra_ini, $_SESSION["reemplazar"], $cadena_ini);

				$sql="	UPDATE ".$tabla."
						SET ".$campo." = '".$cadena."'
						WHERE ".$campo." = '".$cadena_ini."'";
				$resultado = $conexion->query($sql);
				//echo "<h1>Sutituido ".$palabra_ini." por ".$_SESSION["reemplazar"]."</h1>";
			}

			return $cadena;
	}
	//Si es la 1a vez que entro (sin pasar variables por formulario) y la variable de sesión está vacía, pinto el formulario de acceso-conexión:
	if(empty($_POST) && empty($_SESSION['servidor'])){

		?>

		<form name="forma" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">

			<table>
				<tr>
					<td>Servidor/URL</td> <td><input type="text" name="servidor" value="localhost"><td>
				</tr>
				<tr>
					<td>Base de Datos</td> <td><input type="text" name="bbdd" value="efprod"><td>
				</tr>
				<tr>
					<td>Usuario</td> <td><input type="text" name="usuario" value="root"><td>
				</tr>
				<tr>
					<td>Password</td> <td><input type="password" name="password" value="Bilb0Db!"><td>
				</tr>
				<tr>
					<td>Reemplazar</td> <td><input type="text" name="reemplazar" value=""><input type="checkbox" name="s_reemplazar" value="Bike"><td>
				</tr>
				<tr>
					<td><input type="submit" name="abrir" value="Abrir BBDD"><td>
				</tr>
			</table>		

		</form>

		<?php

		//salgo de la ejecución:
		exit();

	}

		//------------------------------------------------------------------------------------------

		//2a entrada, con campos del formulario POST llenos:
	else{ 
		//limpiamos los campos
		if(isset($_POST['servidor'])){
				$_SESSION['servidor']		=	addslashes(trim($_POST['servidor']));
				$_SESSION['usuario']		=	addslashes(trim($_POST['usuario']));
				$_SESSION['password']		=	addslashes(trim($_POST['password']));
				$_SESSION['bbdd']			=	addslashes(trim($_POST['bbdd']));
				if (!empty($_POST["reemplazar"] && $_POST["s_reemplazar"])) {
					$_SESSION['reemplazar'] = $_POST["reemplazar"];
				}else{
					$_SESSION['reemplazar']=false;
				}
		}		

		//Muestro mensaje de que se va a remplazar si se da el caso
		if ($_SESSION['reemplazar']) {
			?>
				<div style="background: #ff6e6e;position: fixed;width: 250px;margin-left: -125px;left: 50%;text-align: center;top: 0px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px;padding-top: 20px;color: #fffeeb;height: 40px;font-size: 18px;">
					Se va a reemplazar por: <b style="color: white;"><i><?php echo $_SESSION['reemplazar']; ?></b></i>
				</div>
			<?php
		}

		//echo "Declarada<br>";

		//conectamos a la BBDD:
		$conexion=mysqli_connect($_SESSION['servidor'],$_SESSION['usuario'],$_SESSION['password'], $_SESSION['bbdd']) or exit('Error en la conexi&oacute;n<br><a href="'.$_SERVER['PHP_SELF'].'?salir=true">Volver</a>');

		//seleccionamos cierta base de datos:
		//$seleccion=mysql_select_db() or exit('Error en la seleccion de la BBDD<br><br><a href="'.$_SERVER['PHP_SELF'].'?salir=true">Volver</a>');

		//si logramos conectar lo registramos en la sesion:	
		$_SESSION['conectado']=($conexion!==FALSE)? TRUE : FALSE;


		//Estamos conectados pero aun no hemos enviado la busqueda. Vemos las tablas de la BBDD en cuestion llamando a explora_BD()
		if($_SESSION['conectado'] && (!isset($_POST['palabra']) || isset($_POST['abrir'])) ){
				
				if ($seleccion!==FALSE){
				
						echo 'Conexi&oacute;n establecida como <b style="color: #ff6e6e; cursor: pointer;">'.$_SESSION['usuario']."@".$_SESSION['servidor']."</b> en ".$_SESSION['bbdd'].'.<hr color="#992222">';
						explora_BD($conexion);			
				}
		
		}
		//Ya hemos explorado y pasamos un formulario con un array $_POST['tabla'] generado por los checkboxes de tablas.
		else{
				
				//registro el tiempo inicial:
				$tiempo_ini=microtime();

				//Abro una a una las tablas y miro entre sus columnas buscando el valor pasado:
				//echo var_dump($_POST['tabla']);

				echo 'Conexi&oacute;n establecida como <b style="color: #ff6e6e; cursor: pointer;">'.$_SESSION['usuario']."@".$_SESSION['servidor']."</b> en ".$_SESSION['bbdd'].'.<hr color="#992222">';

				foreach($_POST['tabla'] AS $indice => $valor){

						//$valor es el nombre de la tabla

						$cad="SHOW COLUMNS FROM ".$valor;//$_SESSION['bbdd'].".".$valor; 

						//echo $cad."<br>";
						
						$que=$conexion->query($cad) or exit("ERR: ".$cad);

						$cad_or="SELECT * FROM ".$valor." WHERE ";

						$nombre_columna=Array();

						//Monto una cadena SQL con ORs por cada columna hallada en la tabla: WHERE columna1 LIKE "%%" OR columna2 LIKE "%%"...
						while($fila=$que->fetch_array(MYSQLI_NUM)){

								//Genero array de nombres de columna:
								$nombre_columna[]=$fila[0];
								//empalmo cadena de SQL
								$cad_or.=" ".$fila[0]." LIKE '%".$_POST['palabra']."%' OR ";

								//echo $cad_or."<br>";

						}

						//Ajusto la cadena SQL creada, limpiando el último OR que sobra:
						$cad_or=substr($cad_or, 0,-3);
						
						//echo $cad_or;

						//Consulto
						$quet=$conexion->query($cad_or); 

						//Resultados de busqueda en todas las columnas de esta tabla.
						if($quet->num_rows > 0){

							//echo $quet->num_rows;

							$valor_actual="0_primera_tabla_!?*";
							$ultimo_valor="0_ultima_tabla_!?*";

								while($filat=$quet->fetch_array(MYSQLI_NUM)){

									$valor_actual=$valor;

									if($ultimo_valor==$valor_actual){

										echo '<tr>';
										for($ind=0;$ind<count($filat);$ind++){
												// Pinto contenido del registro resaltando en color la palabra buscada:
												echo '<td bgcolor="#bfbfbf" nowrap="true">'.remarcar($conexion, $valor, $nombre_columna[$ind], $filat[$ind])."</td>";
										}	
										echo '</tr>';

									}else{

										//Pinto una cabecera:

										echo '<br><table width="100%"><tr><td colspan="'.count($nombre_columna).'">';
										echo '<h3>Buscando en la tabla <span style="color: #a2a2a2;">'.$valor.'</span>:</h3>';
										echo '</td></tr>';

										//Pinto los nombres de las columnas con el array generado antes:
										echo '<tr>';

										for($j=0;$j < count($nombre_columna);$j++){
													echo '<td bgcolor="#ff6e6e" style="padding: 5px 10px;"><b><font face="arial" size="2" color="#ffffff">'.$nombre_columna[$j]."</font></b></td>";
										}

										echo "</tr>";
					
										echo '<tr>';
										for($ind=0;$ind<count($filat);$ind++){
												// Pinto contenido d#bfbfbfl registro resaltando en color la palabra buscada:
												echo '<td bgcolor="#bfbfbf" nowrap="true">'.remarcar($conexion, $valor, $nombre_columna[$ind], $filat[$ind])."</td>";
										}	
										echo '</tr>';

										global $ultimo_valor;
										$ultimo_valor=$valor;
									}
		
								}
						}
				
						echo '</table>';

				}

				//Pinto un enlace para volver a buscar de nuevo
				echo '<br><hr color="#992222"><br><a href="'.$_SERVER['PHP_SELF'].'">Volver a buscar</a> <a href="'.$_SERVER['PHP_SELF'].'?salir=true" class="salir_bbdd">Salir de BBDD</a><br>';
						
				//Capturo el tiempo de nuevo.		
				$tiempo_fin=microtime();
		
				//Calculo y pinto estadísticas:
				echo "<br>Se ha tardado ";
				printf("%f",($tiempo_fin - $tiempo_ini));
				echo " segundos.";
					
	
		}


}


//--------------------------------------------------------------------------------------------------


//Funcion que explora la BBDD y pinta sus tablas:

	function explora_BD($conexion){
		
		?>
		<script type="text/javascript">

			var marcados=true;
			function marcatodos(){
				if(marcados==false){
					for(i=0;i<document.forma.elements.length;i++){
					
						if(document.forma.elements[i].type=='checkbox')	document.forma.elements[i].checked=true;
					
					}
					marcados=true;
					document.getElementById("checkAllTables").classList.add('nocheck');
				}else{
					for(i=0;i<document.forma.elements.length;i++){
					
						if(document.forma.elements[i].type=='checkbox')	document.forma.elements[i].checked=false;
					
					}
					marcados=false;
					document.getElementById("checkAllTables").classList.remove('nocheck');
				}
			}

			function quitarCache(){
				let button=document.getElementById("quitarCacheButton");
				for(i=0;i<document.forma.elements.length;i++){
					if(document.forma.elements[i].type=='checkbox' && 
							(	document.forma.elements[i].value.indexOf("cache") > -1 || 
								document.forma.elements[i].value.indexOf("_field_revision_field_") > -1 || 
								document.forma.elements[i].value.indexOf("watchdog") > -1  ||
								document.forma.elements[i].value.indexOf("access_log") > -1 ) ){	
						document.forma.elements[i].checked=button.checked;
					}				
				}
				if (button.checked) {
					button.checked=false;
					button.classList.remove('nocheck');
				}else{
					button.checked=true;
					button.classList.add('nocheck');
				}
			}
			$(document).ready(function(){
				quitarCache();
			})

		</script>

			<form name="forma" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
				<div style="margin-bottom: 15px;" id="manage_tables">
					<input type="button" name="" value="Check All Tables" onclick="marcatodos()" id="checkAllTables" class="nocheck">
					<input type="button" name="" value="Remove Cache Tables (Drupal)" onclick="quitarCache()" id="quitarCacheButton" class="nocheck">
				</div>
				<input type="submit" name="buscar" value="Buscar" style="background: #ff6e6e; border: white; border-radius:  10px; padding: 10px; width:  85px; color: white; font-size:  15px; cursor:  pointer;"><br><br>
				<input type="text" name="palabra" value="" style="outline: none; background: #fffcd4; border: 1px solid #dedbb9; padding: 5px; border-radius:  8px; color: #ff6e6e; "> Palabra <sup>(*) Insensible a Mayusculas/Minusculas, sensible a acentos</sup><br><br>
		<?php

			//Consulto y pinto las tablas de la BBDD en cuestion:

			$cad="SHOW TABLES";
			$que=$conexion->query($cad);

			$i=0;
			while($fila=$que->fetch_array(MYSQLI_NUM)){
					echo '<input type="checkbox" name="tabla[]" value="'.$fila[0].'" checked="checked">'." - ".$fila[0]."<br>\n";
					$i++;

			}


			//Pinto formulario de busqueda en BBDD:

			echo '
			<br>
			<br><input type="submit" name="buscar" value="Buscar" style="background: #ff6e6e; border: white; border-radius:  10px; padding: 10px; width:  85px; color: white; font-size:  15px; cursor:  pointer;"></form>
			<hr color="#992222">
			<a href="'.$_SERVER['PHP_SELF'].'?salir=true" class="salir_bbdd">Salir de BBDD</a>
			';

	}


?>